package com.eragtraining;


import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;


public class ReadFromFolder {

    private static final Logger log = Logger.getLogger (ReadFromFolder.class.getName ());


    public static void listFilesForFolder(final File folder) {
        try {
            for (final File fileEntry : folder.listFiles()) {
                if (fileEntry.isDirectory()) {
                    listFilesForFolder(fileEntry);
                } else {
                    String nameOfFile = fileEntry.getName();
                    log.info(" получено имя файла: " + nameOfFile);
                    String extensionOfFile = getFileExtension(nameOfFile);
                    log.info(" получено расщирение файла: " + extensionOfFile);

                    if (extensionOfFile.equals(".txt")) {
                        byte[] bytes = Files.readAllBytes(Paths.get(fileEntry.getPath()));
                        String content = new String(bytes, StandardCharsets.UTF_8);
                        System.out.println(content);
                        log.info(" получен текстовый файл ");
                    } else if (extensionOfFile.equals(".png")) {
                        System.out.println("Контент не может быть выведен");
                        log.info(" получено .png-изображение");
                    }
                }
            }

        }catch (IOException ex){
            System.out.println("ошибка");
            log.severe("IOException");
        }catch (NullPointerException nullEx){
            System.out.println("ошибка");
            log.severe("NullPointerException ");
        }
    }

    private static String getFileExtension(String mystr) {
        int index = mystr.indexOf('.');
        return index == -1? null : mystr.substring(index);
    }



}
